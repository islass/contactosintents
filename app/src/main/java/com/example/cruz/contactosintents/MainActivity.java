package com.example.cruz.contactosintents;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    TextView tvDatos;
    ListView lvContactos;
    Button btnActualizar, btnAgreagarnuevo;
    ImageView ivFoto;

    //static String baseUrl = "http://192.168.8.103:8080/api/values";  // Esta es la URL de la API
    static String baseUrl = "http://192.168.43.100:8080/api/values";
    ArrayList<Contactos> contactos = new ArrayList<>();
    ListaPersonalizada adaptador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        tvDatos = findViewById(R.id.tvDatos);
        tvDatos.setMovementMethod(new ScrollingMovementMethod());
        lvContactos = findViewById(R.id.lvContactos);
        btnActualizar = findViewById(R.id.btnActualizar);
        btnAgreagarnuevo = findViewById(R.id.btnAgregarNuevo);
        ivFoto = findViewById(R.id.foto);

        adaptador = new ListaPersonalizada(this, R.layout.listapersonalizada, contactos);
        lvContactos.setAdapter(adaptador);

        new GetContacto(baseUrl, contactos, adaptador).execute();

        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new GetContacto(baseUrl, contactos, adaptador).execute();
            }
        });

        lvContactos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Contactos contacto = (Contactos) adapterView.getItemAtPosition(i);
                Intent intent = new Intent(getApplicationContext(), DetallesContacto.class);
                intent.putExtra("nombre",contacto.getNomnbre());
                intent.putExtra("telefonos",contacto.getTelefono());
                intent.putExtra("foto",contacto.getFoto());
                startActivity(intent);

                Toast.makeText(getApplicationContext(), contacto.nomnbre, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
