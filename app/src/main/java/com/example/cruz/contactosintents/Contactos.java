package com.example.cruz.contactosintents;

/**
 * Created by Laura on 25/02/2019.
 */

public class Contactos {
    String nomnbre;
    String Telefono;
    String Foto;

    public Contactos(String nomnbre, String telefono, String foto) {
        this.nomnbre = nomnbre;
        Telefono = telefono;
        Foto = foto;
    }

    public Contactos() {
    }

    public String getNomnbre() {
        return nomnbre;
    }

    public String getTelefono() {
        return Telefono;
    }

    public String getFoto() {
        return Foto;
    }

}

