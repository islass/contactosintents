package com.example.cruz.contactosintents;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.widget.ImageView;
import android.widget.TextView;

public class DetallesContacto extends AppCompatActivity {

    ImageView ivFoto ;
    TextView tvNombre,tvTelefonos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles_contacto);

        ivFoto=findViewById(R.id.ivFoto);
        tvNombre=findViewById(R.id.tvNombre);
        tvTelefonos=findViewById(R.id.tvNumeros);
        Intent intent =getIntent();

        String foto =intent.getStringExtra("foto");
        byte[] fotoBytes;
        if (foto!=null ) {
            if (!foto.equals("")) {
                String fotobase64 = intent.getStringExtra("foto");;
                if (!fotobase64.equals(null)) {
                    fotoBytes = Base64.decode(fotobase64, Base64.DEFAULT);
                    ivFoto.setImageBitmap(BitmapFactory.decodeByteArray(fotoBytes, 0, fotoBytes.length));
                }
            }
        } else {
            ivFoto.setImageResource(R.drawable.contacto);
        }
        tvNombre.setText(intent.getStringExtra("nombre"));
        tvTelefonos.setText(intent.getStringExtra("telefonos"));
    }
}
