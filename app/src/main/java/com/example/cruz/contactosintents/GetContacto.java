package com.example.cruz.contactosintents;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;


/**
 * Created by Laura on 20/02/2019.
 */

public class GetContacto extends AsyncTask<Void,Void,Void> {

    private String url;
    private ArrayList<Contactos> contactos ;
    private ListaPersonalizada adapter ;

    GetContacto(String url, ArrayList<Contactos> contacto, ListaPersonalizada adapter) {
        this.url = url;
        this.contactos= contacto;
        this.adapter= adapter;
    }



    @Override
    protected Void doInBackground(Void... voids) {
        try {

            URL uri = new URL(url);
            HttpURLConnection conexion = (HttpURLConnection) uri.openConnection();
            conexion.setRequestMethod("GET");
            InputStream entradaDatos = conexion.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(entradaDatos));
            String linea="";
            StringBuilder respuesta = new StringBuilder();
            while ((linea = reader.readLine()) != null)
            respuesta.append(linea);

            JSONArray contacto = new JSONArray(respuesta.toString());

            ArrayList<Contactos> contactos = new ArrayList<>();

            for (int g = 0; g < contacto.length(); g++) {

                Contactos personaTemp = new Contactos();

                JSONObject objetoContacto = contacto.getJSONObject(g);
                JSONArray telefonos = new JSONArray(objetoContacto.getString("telefonos"));
                personaTemp.nomnbre = objetoContacto.getString("nombre");
                personaTemp.Foto = objetoContacto.getString("Foto");
                personaTemp.Telefono = "";


                for (int i = 0; i < telefonos.length(); i++) {
                    JSONObject objetoTelefonos = telefonos.getJSONObject(i);
                    personaTemp.Telefono = "\n Telefono[" + i + "]" + objetoTelefonos.get("numero");
                }
                contactos.add(personaTemp);
            }
            this.contactos.addAll(contactos);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    return  null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        adapter.notifyDataSetChanged();
        super.onPostExecute(aVoid);
    }
}
